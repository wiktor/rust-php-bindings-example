extern crate solder;

use solder::*;
use solder::zend::*;
use solder::info::*;

use std::{io::Write, time::SystemTime};

use anyhow::Context;

use sequoia_openpgp as openpgp;

use openpgp::{Cert, serialize::{SerializeInto, stream::{Armorer, Signer}}};
use openpgp::types::KeyFlags;
use openpgp::parse::Parse;
use openpgp::serialize::stream::{
    Message, LiteralWriter, Encryptor,
};
use openpgp::policy::StandardPolicy as P;

#[no_mangle]
pub extern fn php_module_info() {
    print_table_start();
    print_table_row("Bindings for OpenPGP library Sequoia", "enabled");
    print_table_end();
}

#[no_mangle]
pub extern fn get_module() -> *mut zend::Module {
    let sequoia_encrypt_fn = FunctionBuilder::new(c_str!("anonaddy_sequoia_encrypt"), sequoia_encrypt)
        .with_arg(ArgInfo::new(c_str!("signing_cert"), 0, 0, 0))
        .with_arg(ArgInfo::new(c_str!("content"), 0, 0, 0))
        .with_arg(ArgInfo::new(c_str!("recipient_cert"), 0, 0, 0))
        .build();
    let sequoia_merge_fn = FunctionBuilder::new(c_str!("anonaddy_sequoia_merge"), sequoia_merge)
        .with_arg(ArgInfo::new(c_str!("existing_cert"), 0, 0, 0))
        .with_arg(ArgInfo::new(c_str!("new_cert"), 0, 0, 0))
        .build();
    let sequoia_minimize_fn = FunctionBuilder::new(c_str!("anonaddy_sequoia_minimize"), sequoia_minimize)
        .with_arg(ArgInfo::new(c_str!("cert"), 0, 0, 0))
        .build();
    ModuleBuilder::new(c_str!("sequoia"), c_str!("0.1.0-dev"))
        .with_info_function(php_module_info)
        .with_function(sequoia_encrypt_fn)
        .with_function(sequoia_merge_fn)
        .with_function(sequoia_minimize_fn)
        .build()
        .into_raw()
}


#[no_mangle]
pub extern fn sequoia_encrypt(_data: &ExecuteData, retval: &mut Zval) {
    let mut signing_cert_zval = Zval::new_as_null();
    let mut recipient_cert_zval = Zval::new_as_null();
    let mut content_zval = Zval::new_as_null();

    php_parse_parameters!(&mut signing_cert_zval, &mut recipient_cert_zval, &mut content_zval);

    let signing_cert = String::try_from(signing_cert_zval).ok().unwrap();
    let content = String::try_from(content_zval).ok().unwrap();
    let recipient_cert = String::try_from(recipient_cert_zval).ok().unwrap();

    let hello = encrypt_for(signing_cert, recipient_cert, content).unwrap_or(String::from("something bad happened"));

    php_return!(retval, hello);
}


fn encrypt_for(signing_cert: String, recipient_cert: String, content: String) -> openpgp::Result<String> {
    let p = &P::new();

    let mode = KeyFlags::empty().set_storage_encryption().set_transport_encryption();

    let signing_cert = Cert::from_bytes(&signing_cert)?
    .keys().unencrypted_secret()
    .with_policy(p, None).alive().revoked(false).for_signing()
    .nth(0).unwrap().key().clone().into_keypair()?;

    let certs: Vec<openpgp::Cert> = vec![Cert::from_bytes(&recipient_cert)?];

    let mut recipients = Vec::new();
    for cert in certs.iter() {
        let mut found_one = false;
        for key in cert.keys().with_policy(p, None)
            .alive().revoked(false).key_flags(&mode)
        {
            recipients.push(key);
            found_one = true;
        }

        if ! found_one {
            return Err(anyhow::anyhow!("No suitable encryption subkey for {}",
                                       cert));
        }
    }

    let mut sink = vec![];

    let message = Message::new(&mut sink);

    let message = Armorer::new(message).build()?;

    let message = Encryptor::for_recipients(message, recipients)
        .build().context("Failed to create encryptor")?;

    let message = Signer::new(message, signing_cert).build()?;

    let mut message = LiteralWriter::new(message).build()
        .context("Failed to create literal writer")?;

    message.write_all(content.as_bytes())?;

    message.finalize()?;

    Ok(String::from_utf8(sink)?)
}

#[no_mangle]
pub extern fn sequoia_merge(_data: &ExecuteData, retval: &mut Zval) {
    let mut existing_cert = Zval::new_as_null();
    let mut new_cert = Zval::new_as_null();

    php_parse_parameters!(&mut existing_cert, &mut new_cert);

    let existing_cert = String::try_from(existing_cert).ok().unwrap();
    let new_cert = String::try_from(new_cert).ok().unwrap();

    let merged_cert = merge_certs(existing_cert, new_cert).unwrap_or(String::from("something bad happened"));

    php_return!(retval, merged_cert);
}

fn merge_certs(existing_cert: String, new_cert: String) -> openpgp::Result<String> {
    let existing_cert = Cert::from_bytes(&existing_cert)?;
    let new_cert = Cert::from_bytes(&new_cert)?;

    let merged_cert = existing_cert.merge_public(new_cert)?;

    let armored = merged_cert.armored().to_vec()?;

    Ok(String::from_utf8(armored)?)
}

#[no_mangle]
pub extern fn sequoia_minimize(_data: &ExecuteData, retval: &mut Zval) {
    let mut cert = Zval::new_as_null();

    php_parse_parameters!(&mut cert);

    let cert = String::try_from(cert).ok().unwrap();

    let cert = minimize_cert(&cert).unwrap_or(String::from("something bad happened"));

    php_return!(retval, cert);
}

fn minimize_cert(cert: &String) -> openpgp::Result<String> {
    use std::convert::TryFrom;

    let ref policy = P::new();

    let cert = Cert::from_bytes(&cert)?;
    let cert = cert.with_policy(policy, None)?;

    let mut acc = Vec::new();

    let c = cert.primary_key();
    acc.push(c.key().clone().into());

    for s in c.self_signatures()   { acc.push(s.clone().into()) }
    for s in c.self_revocations()  { acc.push(s.clone().into()) }

    for c in cert.userids() {
        acc.push(c.userid().clone().into());
        for s in c.self_signatures()   { acc.push(s.clone().into()) }
        for s in c.self_revocations()  { acc.push(s.clone().into()) }
    }

    let flags = KeyFlags::empty().set_storage_encryption().set_transport_encryption();

    for c in cert.keys().subkeys().key_flags(flags).revoked(false) {
        if c.key_expiration_time().map(|time| time > SystemTime::now()).unwrap_or(true) {
            acc.push(c.key().clone().into());
            for s in c.self_signatures()   { acc.push(s.clone().into()) }
            for s in c.self_revocations()  { acc.push(s.clone().into()) }
        }
    }

    let filtered_cert = Cert::try_from(acc)?;

    let armored = filtered_cert.armored().to_vec()?;

    Ok(String::from_utf8(armored)?)
}

#[test]
fn end_to_end_test() -> openpgp::Result<()> {
    let existing_cert: String = String::from_utf8_lossy(include_bytes!("../wiktor-expired.asc")).to_string();
    let new_cert: String = String::from_utf8_lossy(include_bytes!("../wiktor-fresh.asc")).to_string();

    // merging certs should work
    let cert = merge_certs(existing_cert, new_cert)?;
    assert!(Cert::from_bytes(&cert).is_ok());

    // minimizing certs should provide a valid cert that is smaller than the input
    let minimized_cert = minimize_cert(&cert)?;
    assert!(Cert::from_bytes(&minimized_cert).is_ok());
    assert!(minimized_cert.len() < cert.len());

    // encrypting should still work
    let signing_cert = String::from_utf8_lossy(include_bytes!("../signing-key.asc")).to_string();
    let message = encrypt_for(signing_cert, minimized_cert, "just a random string".to_string())?;
    assert!(openpgp::Message::from_bytes(message.as_bytes()).is_ok());

    Ok(())
}

#[test]
fn minimize() -> openpgp::Result<()> {
    let cert: String = String::from_utf8_lossy(include_bytes!("../wiktor-fresh.asc")).to_string();

    let minimized_cert = minimize_cert(&cert)?;
    assert!(Cert::from_bytes(&minimized_cert).is_ok());
    assert_eq!(4728, minimized_cert.len());

    Ok(())
}
