# Sample AnonAddy Sequoia PHP bindings

DEPRECATED: This project is obsoleted in favor of https://gitlab.com/willbrowning/anonaddy-sequoia that properly supports PHP 8 and uses a library that is maintained.

------

This crate currently works only with PHP 7 due to the bindings library limitations.
If your system has a different version of PHP installed you need to build the crate explicitly requesting PHP 7's API versions:

```
$ php7 -i | grep 'PHP API' | sed -e 's/PHP API => //'
> 20200930
$ php7 -i | grep 'PHP Extension Build' | sed -e 's/PHP Extension Build => //'
> API20190902,NTS
```

And then build with:

```
PHP_API_VERSION=20190902 PHP_EXTENSION_BUILD=API20190902,NTS cargo build --release
```

Run with:

```
php7 -dextension=$(pwd)/target/release/libmylib.so -a
```

## Available functions

### anonaddy_sequoia_encrypt

Encrypts a message and signs it:

```php
$signing_cert = file_get_contents("signing-key.asc");
$recipient_cert = file_get_contents("wiktor.asc");
echo anonaddy_sequoia_encrypt($signing_cert, $recipient_cert, "Hello Bruno!");
```

### anonaddy_sequoia_merge

Merges public information from a certificate:

```php
$current_cert = file_get_contents("wiktor-expired.asc");
$new_cert = file_get_contents("wiktor-fresh.asc");
echo anonaddy_sequoia_merge($current_cert, $new_cert);
```

### anonaddy_sequoia_minimize

Minimizes the certificate leaving only data necessary for `anonaddy_sequoia_encrypt`.

It is advisable to minimize cert that has been merged by `anonaddy_sequoia_merge` to
remove unnecessary data.

```php
$full = file_get_contents("wiktor-full.asc");
echo strlen($full);
# 195709
$min = anonaddy_sequoia_minimize($full);
echo strlen($min);
# 36871
```
